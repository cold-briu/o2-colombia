import React from 'react';
import logo from '../../../assets/logos/02colombia-white.svg';
const Header = () => {
  return (

    <div className="o2-form-container uk-child-width-1-1@m uk-flex-center uk-grid-collapse uk-grid-match  uk-background-primary" uk-grid>
      <div className="uk-card uk-card-body uk-text-center uk-padding-small">
        <div className="uk-flex uk-flex-middle uk-flex-center">
          <a href="/" className="uk-navbar-item uk-logo">
            <img src={logo} alt="O2Colombia" />
                </a>
            <p className="uk-text-lead uk-text-white uk-text-center uk-margin uk-margin-left">
              Unidos podemos con todo, <br/>#UnidosSomosMásPaís
							</p>

							</div>
              </div>
      </div>
  );
}

export default Header;
