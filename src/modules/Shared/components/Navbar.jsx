import React from 'react';
import logo from '../../../assets/logos/02colombia-white.svg';
import Sidebar from './Sidebar'
const Navbar = () => {
  return (
    <>
      <div className="o2-nav">
        <div className="uk-container uk-container-large">
          <nav className="uk-navbar-container uk-navbar-transparent uk-dark" uk-navbar="">
            <div className="uk-navbar-left">
              <a className="uk-navbar-toggle" uk-navbar-toggle-icon="" uk-toggle="target: #o2-offcanvas"></a>
            </div>
            <div className="uk-navbar-center">
              <a href="" className="uk-navbar-item uk-logo">
                <img className="o2-patricinio" src={logo} alt="O2Colombia"/>
                </a>
            </div>
              <div className="uk-navbar-right">
                <small className="uk-text-uppercase">Hola Andrés</small>
                <a className="uk-button uk-button-small" href="#">
                  <span uk-icon="icon: sign-out"></span>
                  Salir
                </a>
              </div>
          </nav>
        </div>
      </div>
      <Sidebar/>
    </>
  );
}

export default Navbar;
