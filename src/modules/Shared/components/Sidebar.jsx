import React from 'react';

const Sidebar = () => {
  return (
    <>

      <div id="o2-offcanvas" uk-offcanvas="mode: push; overlay: true">
        <div className="uk-offcanvas-bar o2-offcanvas-bar">

          <button className="uk-offcanvas-close" type="button" uk-close=""></button>
          <ul className="uk-nav uk-nav-default">
            <li className="uk-nav-header">O2Colombia</li>
            <li className="uk-parent">
              <ul className="uk-nav-sub">
                <li><a href="#"><span className="uk-margin-small-right" uk-icon="icon: heart"></span> Manifesto</a></li>
              </ul>
            </li>

            <li className="uk-nav-header">Solicitudes</li>
            <li className="uk-parent">
              <ul className="uk-nav-sub">
                <li><a href="#"><span className="uk-margin-small-right" uk-icon="icon: thumbnails"></span> Ver todas</a></li>
                <li className="uk-nav-divider"></li>
                <li><a href="#"><span className="uk-margin-small-right" uk-icon="icon: plus-circle"></span> Nueva Solicitud</a></li>
              </ul>
            </li>
            <li className="uk-nav-header">Mi cuenta</li>
            <li className="uk-parent">
              <ul className="uk-nav-sub">
                <li><a href="#"><span className="uk-margin-small-right" uk-icon="icon: user"></span> Perfil</a></li>
                <li className="uk-nav-divider"></li>
                <li><a href="#"><span className="uk-margin-small-right" uk-icon="icon: sign-out"></span> Salir</a></li>
              </ul>
            </li>


          </ul>
        </div>
      </div>
    </>
  );
}

export default Sidebar;
