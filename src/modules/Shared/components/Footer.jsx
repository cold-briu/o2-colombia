import React from 'react';
import logoAndi from '../../../assets/logos/logo-andi.png';
import logoFundaAndi from '../../../assets/logos/logo-fundacionandi.png';
import logoIdata from '../../../assets/logos/logo-idata.png';
import logoKompras from '../../../assets/logos/logo-kompras.png';
import logoEvolution from '../../../assets/logos/logo-evolution.png';
import logoHumanEX from '../../../assets/logos/logo-humanEX.png';

import dataPolicy from '../../../assets/pdf/o2colombia-tratamiento-de-datos.pdf'
import privacyPolicy from '../../../assets/pdf/o2colombia-avisoPrivacidad.pdf'

const Footer = () => {
  return (

    <div className="o2-foot uk-background-dark">
      <div className="uk-container uk-dark">
        <div className="">
          Gracias al apoyo de:
          <img className="o2-patricinio" src={logoAndi} alt=""/>
          <img className="o2-patricinio" src={logoFundaAndi} alt=""/>
          <img className="o2-patricinio" src={logoIdata} alt=""/>
          <img className="o2-patricinio" src={logoKompras } alt=""/>
          <img className="o2-patricinio" src={logoEvolution} alt=""/>
              <img className="o2-patricinio" src={logoHumanEX} alt=""/>
				</div>
              <div className="uk-margin-top uk uk-padding uk-text-meta"  >
                Cualquier transacción que sea causada o propiciada por medio de la plataforma O2 Colombia deberá acogerse a los 				siguientes artículos teniendo en cuenta las regulaciones colombianas pertinentes: Artículo 60 de la ley 81 de 1988 				“ De la Política de Precios”, Artículo 87 de la ley 1438 de 2011 “Comisión Nacional de Precios de Medicamentos y 				Dispositivos Médicos - CNPMD”, Artículo 90 de la ley 1438 de 2011 “Garantía de la Competencia”; de acuerdo a lo 				establecido por el Ministerio de Salud en la Regulación de Precios de Dispositivos Médicos.
				</div>
              <a href={dataPolicy} target="_blank" >Click aquí para ver política de tratamiento de datos</a>
              <br/>
                <br/>
                  <a href={privacyPolicy} target="_blank">Click aquí para ver aviso de privacidad</a>
			</div>

		</div>
  );
}

export default Footer;
