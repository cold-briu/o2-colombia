import React from 'react'
import { Footer, Header  } from "../components";

const publicLayout = ({children}) => (
<div className="page-container">
  <Header />
    {children}
  <Footer />
</div>

)

export default publicLayout