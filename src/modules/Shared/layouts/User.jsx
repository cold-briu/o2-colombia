import React from 'react'
import { Footer, Navbar } from "../components";

const userLayout = ({ children }) => (
  <>
    <Navbar />
  <div className="page-container">
      {children}
    <Footer />
  </div>
</>
)

export default userLayout