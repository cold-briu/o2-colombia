import * as components from './components'
import * as layouts from './layouts'

export default { components, layouts }