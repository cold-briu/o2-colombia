import React from 'react';

import {  Hero  } from "../components";
import shared  from "../../Shared";

const { Footer } = shared.components


 const Landing = () =>{
  return (
    <>
      <div className="page-container uk-background-primary">
        <Hero/>
        <Footer/>
      </div>
    </>

  );
}

export default Landing