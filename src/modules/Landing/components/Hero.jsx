import React, { useState } from 'react';
import logo from '../../../assets/logos/02colombia-white.svg'
import icoPlus from '../../../assets/icons/ico-plus.svg'
import icoHeart from '../../../assets/icons/ico-heart.svg'
import Modal from './Modal'

const Hero = () => {
  const [isModalVisible, setIsModalVisible] = useState(false)
  return (
    <div className="o2-form-container uk-child-width-1-1@m uk-flex-center uk-grid-collapse uk-grid-match" uk-grid="">
      <div className="uk-card uk-card-body uk-text-center">
        <div className="uk-flex uk-flex-middle uk-flex-center">
          <img src={ logo } alt="O2Colombia" />
				</div>
        <p className="uk-text-lead uk-text-white uk-text-center uk-margin">
           Unidos podemos con todo, <br/>#UnidosSomosMásPaís
				</p>
        <ul className="uk-list uk-list-large uk-list-divider uk-text-center uk-margin uk-text-body">
          <li>Somos una plataforma digital sin ánimo de lucro, que a partir del análisis de datos buscamos darle un respiro a las instituciones de salud de Colombia.</li>
          <li>Si eres proveedor de servicios o una institución de salud, inscríbete. Nuestra aplicación los conectará.</li>
        </ul>
        <a className="uk-button uk-button-text uk-button-large uk-margin-top uk-margin-small-bottom" href="/login" uk-toggle="" >
          Ingresar a la plataforma <img src={icoPlus} alt=""/>
        </a>
        <button className="uk-button uk-button-text uk-button uk-margin-small-top" href="#Manifesto" uk-toggle="" onClick={()=>setIsModalVisible(true)} >
          Ver Manifesto O2Colombia <img src={icoHeart} alt=""/>
        </button>
			</div>
      <div id="Manifesto" className="uk-flex-top" uk-modal="" >
        <div className="uk-modal-dialog uk-margin-auto-vertical">
          <button className="uk-modal-close-default" type="button" uk-close=""></button>
          <div className="uk-modal-header">
            <h2 className="uk-modal-title uk-text-aquadark">O2 Colombia Manifesto</h2>
          </div>
          <div className="uk-modal-body">
            Somos una plataforma digital sin ánimo de lucro, fundamentada en análisis de datos, que busca darle un respiro a las entidades que hacen parte del sistema de salud.
									<br /><br />
                          Llegamos como una solución práctica y eficiente que busca que todos los sectores empresariales del país puedan ayudar a resolver las necesidades de insumos, logística y cualquier otro tipo de requerimientos prioritarios que tengan las entidades de salud en los próximos meses.
									<br /><br />
                            De la mano de la ANDI y otros actores importantes a nivel nacional, unificamos las capacidades de suministro y respuesta a las instituciones de salud, a nuestros médicos, personal de enfermería y personal de apoyo para enfrentar el enorme reto que nos plantea el COVID19.
									<br /><br />
                              Somos la unión de muchas fuerzas porque queremos que  #RespiremosJuntos
									<br /><br />
                                Si eres proveedor de servicios logísticos o productos que brinden soluciones al sector salud y puedes ayudarnos a darle un respiro al país, inscríbete para hacer parte. Millones de personas te lo agradecerán.
									<br /><br />
                                  Si eres una entidad de salud, nos encontramos listos para responder a tu llamado y soportar rápidamente los requerimientos que puedas tener.
									<br /><br />
                                    Unidos podemos con todo, #UnidosSomosMásPaís
								</div>
        </div>
      </div>
  </div>
  );
}

export default Hero;
