import React from 'react';

const Modal = () => {

  return (


      <div id="Manifesto" className="uk-flex-top" uk-modal="" >
        <div className="uk-modal-dialog uk-margin-auto-vertical">
          <button className="uk-modal-close-default" type="button" uk-close=""></button>
          <div className="uk-modal-header">
            <h2 className="uk-modal-title uk-text-aquadark">O2 Colombia Manifesto</h2>
          </div>
          <div className="uk-modal-body">
            Somos una plataforma digital sin ánimo de lucro, fundamentada en análisis de datos, que busca darle un respiro a las entidades que hacen parte del sistema de salud.
									<br /><br />
                          Llegamos como una solución práctica y eficiente que busca que todos los sectores empresariales del país puedan ayudar a resolver las necesidades de insumos, logística y cualquier otro tipo de requerimientos prioritarios que tengan las entidades de salud en los próximos meses.
									<br /><br />
                            De la mano de la ANDI y otros actores importantes a nivel nacional, unificamos las capacidades de suministro y respuesta a las instituciones de salud, a nuestros médicos, personal de enfermería y personal de apoyo para enfrentar el enorme reto que nos plantea el COVID19.
									<br /><br />
                              Somos la unión de muchas fuerzas porque queremos que  #RespiremosJuntos
									<br /><br />
                                Si eres proveedor de servicios logísticos o productos que brinden soluciones al sector salud y puedes ayudarnos a darle un respiro al país, inscríbete para hacer parte. Millones de personas te lo agradecerán.
									<br /><br />
                                  Si eres una entidad de salud, nos encontramos listos para responder a tu llamado y soportar rápidamente los requerimientos que puedas tener.
									<br /><br />
                                    Unidos podemos con todo, #UnidosSomosMásPaís
								</div>
        </div>
      </div>
);
}

export default Modal;
