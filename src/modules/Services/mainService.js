import axios from 'axios';

import config from '../../config';

const api = axios.create({
  baseURL: config.api.host,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
 });

export default api;
