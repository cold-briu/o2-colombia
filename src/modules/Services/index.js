import api from './mainService'

const paths = {
  tickets: '/tickets',
};

const createErrorResponse = (error) => error ??  null

const handleApiResponse = (status = 0, success = false, data = null, error = createErrorResponse({ details: 'no error set' })) => ({ status,  success, data, error })

// general error
const handleError = (error) => {
  // console.log(error);
  return handleApiResponse(0, false, null, error)
}

const parseFirestoreObject = (data)=> {
  let parsed = []
  for (let [key, value] of Object.entries(data)) {
    parsed.push({...value, databaseId: key})
  }

return parsed
}

export default { api, handleError, handleApiResponse, paths, parseFirestoreObject }