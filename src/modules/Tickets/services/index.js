import mainService from "../../Services/";
const { handleError, handleApiResponse, api, paths, parseFirestoreObject } = mainService


// CREATE TICKET PAYLOAD TYPE

// id: string;
// name: string;
// requiredUnits: number;
// inventoryUnits: number;
// measurementUnit: string;
// priorityId: string;
// categoryId: string;
// statusId: string;
// userId: string;
// agentId: string;

export const create = async (payload) => {
  try {
    const response = await api.post(paths.tickets, payload)
    return handleApiResponse(response.status, true, response.data)
  } catch (e) {
    handleError(e)
  }
}


export const getAll = async()=>{
  try {
    const response = await api.get(paths.tickets)
    return handleApiResponse(response.status, true, parseFirestoreObject(response.data.tickets))
  } catch (e) {
    handleError(e)
  }
}

export const getById = async(id)=>{
  try {
    const response = await api.get(`${paths.tickets}/${id}`)
    return handleApiResponse(response.status, true, response.data)
  } catch (e) {
    handleError(e)
  }
}

export const updateById = async(id, payload)=>{
  try {
    const response = await api.patch(`${paths.tickets}/${id}`, payload)
    return handleApiResponse(response.status, true, response.data)
  } catch (e) {
    handleError(e)
  }
}

export const deleteById = async(id)=>{
  try {
    const response = await api.delete(`${paths.tickets}/${id}`)
    return handleApiResponse(response.status, true, response.data)
  } catch (e) {
    handleError(e)
  }
}

