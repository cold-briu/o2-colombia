import React, { useEffect, useState } from 'react';
import { getAll } from "../services";
const Dashboard = () => {

  const [tickets, setTickets] = useState()
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState()

  useEffect(() => {

    const getTickets = async () => {
      setIsLoading(true)
      const res = await getAll()
      setIsLoading(false)
      if (res && res.success) {
        setTickets(res.data)
        return
      }
      setError(res)
    }

    if (!tickets && !isLoading) {
      getTickets()
    }

  }, [isLoading, tickets])




  if (error) {
    return <h1>Error calling api</h1>
  }

  // PLACE LAYOUT HERE

  return (null);
}

export default Dashboard