import React from 'react'
import icoPlusWhite from '../../../assets/icons/ico-plus-white.svg'
import { constants } from '../../../router'
const { routes } = constants

const dashboardMain = ()=>{
  return (
    <>

      <div className="o2-main uk-section uk-section-small uk-section-default">

        <div className="uk-container uk-container-large">

          <div className="o2-dashboard">
            <div className="o2-header uk-flex uk-flex-between uk-flex-middle">
              <h1 className="uk-heading-small uk-text-aqua">
                Solicitudes
                <a className="uk-button uk-button-primary uk-button-large o2-button" href={routes.create}>
                  Crear Nueva
                  <img src={icoPlusWhite} alt=""/>
                </a>
						</h1>
                <form className="uk-search uk-search-default">
                  <span uk-search-icon=""></span>
                  <input className="uk-search-input" type="search" placeholder="Buscar..."/>
						</form>
					</div>
                <div>
                  <div className="uk-flex uk-flex-middle uk-flex-between uk-background-secondary uk-padding-small">
                    <ul className="uk-subnav uk-subnav-divider uk-margin-remove">
                      <li><a href="#">Solicitudes <span className="uk-badge">29</span></a></li>
                      <li><a href="#">Completadas <span className="uk-badge">20</span></a></li>
                      <li><a href="#">En proceso <span className="uk-badge">9</span></a></li>
                    </ul>
                  </div>

                  <table className="uk-table uk-table-small uk-table-divider uk-table-striped uk-table-hover uk-margin-remove sortable">
                    <thead>
                      <tr>
                        <th className="table-id">Orden</th>
                        <th className="table-state">Estado</th>
                        <th className="uk-table-expand">Producto</th>
                        <th className="table-date">Fecha</th>
                        <th className="table-update">Última modificación</th>
                        <th className="table-user">Asignado</th>
                        <th className="table-options">Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td className="table-id">68</td>
                        <td className="table-state"><span className="uk-label uk-label-info">Nuevo</span></td>
                        <td className="uk-table-expand"><a className="uk-button-link" href={routes.details}>Ropa Quirúrgica</a></td>
                        <td className="table-date">2020-01-10</td>
                        <td className="table-update">Hace 7 horas</td>
                        <td className="table-user"><a className="uk-button-link" href="#">Alejandro</a></td>
                        <td className="table-options">
                          <a href="#" className="uk-icon-button" uk-icon="icon: file-edit" uk-tooltip="title: Detalle"></a>
                          <a href="#" className="uk-icon-button" uk-icon="icon: trash" uk-tooltip="title: Eliminar"></a>
                        </td>
                      </tr>
                      <tr>
                        <td className="table-id">65</td>
                        <td className="table-state"><span className="uk-label">En curso</span></td>
                        <td className="uk-table-expand"><a className="uk-button-link" href={routes.details}>Guantes de latex</a></td>
                        <td className="table-date">2020-01-10</td>
                        <td className="table-update">Hace 7 horas</td>
                        <td className="table-user"><a className="uk-button-link" href="#">Alejandro</a></td>
                        <td className="table-options">
                          <a href="#" className="uk-icon-button" uk-icon="icon: file-edit" uk-tooltip="title: Detalle"></a>
                          <a href="#" className="uk-icon-button" uk-icon="icon: trash" uk-tooltip="title: Eliminar"></a>
                        </td>
                      </tr>
                      <tr>
                        <td className="table-id">63</td>
                        <td className="table-state"><span className="uk-label uk-label-warning">En espera</span></td>
                        <td className="uk-table-expand"><a className="uk-button-link" href={routes.details}>Insumos cirugías</a></td>
                        <td className="table-date">2020-01-09</td>
                        <td className="table-update">Hace 7 horas</td>
                        <td className="table-user"><a className="uk-button-link" href="#">Alejandro</a></td>
                        <td className="table-options">
                          <a href="#" className="uk-icon-button" uk-icon="icon: file-edit" uk-tooltip="title: Detalle"></a>
                          <a href="#" className="uk-icon-button" uk-icon="icon: trash" uk-tooltip="title: Eliminar"></a>
                        </td>
                      </tr>
                      <tr>
                        <td className="table-id">63</td>
                        <td className="table-state"><span className="uk-label uk-label-closed">Cerrado</span></td>
                        <td className="uk-table-expand"><a className="uk-button-link" href={routes.details}>Insumos para seguridad de pacientes</a></td>
                        <td className="table-date">2020-01-08</td>
                        <td className="table-update">Hace 7 horas</td>
                        <td className="table-user"><a className="uk-button-link" href="#">Alejandro</a></td>
                        <td className="table-options">
                          <a href="#" className="uk-icon-button" uk-icon="icon: file-edit" uk-tooltip="title: Detalle"></a>
                          <a href="#" className="uk-icon-button" uk-icon="icon: trash" uk-tooltip="title: Eliminar"></a>
                        </td>
                      </tr>
                      <tr>
                        <td className="table-id">60</td>
                        <td className="table-state"><span className="uk-label uk-label-closed">Cerrado</span></td>
                        <td className="uk-table-expand"><a className="uk-button-link" href={routes.details}>Insumos para seguridad de pacientes</a></td>
                        <td className="table-date">2020-01-06</td>
                        <td className="table-update">Hace 7 horas</td>
                        <td className="table-user"><a className="uk-button-link" href="#">Alejandro</a></td>
                        <td className="table-options">
                          <a href="#" className="uk-icon-button" uk-icon="icon: file-edit" uk-tooltip="title: Detalle"></a>
                          <a href="#" className="uk-icon-button" uk-icon="icon: trash" uk-tooltip="title: Eliminar"></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>

                </div>
                <div className="uk-text-center uk-margin-large">
                  <hr/>
                    <ul className="uk-pagination">
                      <li><a href="#"><span className="uk-margin-small-right" uk-pagination-previous=""></span> Anterior</a></li>
                      <li className="uk-margin-auto-left"><a href="#">Siguiente <span className="uk-margin-small-left" uk-pagination-next=""></span></a></li>
                    </ul>
				</div>


                </div>
			</div>
            </div>
    </>
  )
}
export default dashboardMain