import React from 'react';


const Create = () => {
  return (
    <>
      <div class="o2-main uk-section uk-section-small uk-section-default">

        <div class="uk-container uk-container-large">
          <ul class="uk-breadcrumb">
            <li><a href="#">Solicitudes</a></li>
            <li><span>Nueva</span></li>
          </ul>
          <div class="o2-dashboard">
            <hr/>
              <div class="o2-header uk-flex uk-flex-between uk-flex-middle">
                <h1 class="uk-heading-small uk-text-aqua uk-margin-remove">
                  Nueva Solicitud
						</h1>
              </div>
              <hr/>



						<div class="o2-form-container uk-padding">

                  <form class="uk-form-horizontal o2-form uk-flex uk-flex-left uk-flex-top uk-grid-collapse" uk-grid="">
                    <div class="uk-width-large">
                      <legend class="uk-legend-small uk-text-gray uk-margin">Información básica</legend>
                      <div class="uk-margin">
                        <label class="uk-form-label">Tipo de producto</label>
                        <div class="uk-form-controls">
                          <select class="uk-select uk-form-width-small" id="form-horizontal-select" required="">
                            <option>Escoger...</option>
                            <option value="1">Option 01</option>
                            <option value="2">Option 02</option>
                          </select>
                        </div>
                        <div class="validation-error uk-text-danger uk-hidden"><span class="uk-icon uk-icon-image"><img src="../images/icons/ico-error.svg" alt=""/>></span> Información requerida</div>
                        </div>
                        <div class="uk-margin">
                          <label class="uk-form-label">Cantidad necesaria</label>
                          <div class="uk-form-controls">
                            <input class="uk-input uk-form-width-small" required type="number" placeholder="ejm:24" required/>
										</div>
                            <div class="validation-error uk-text-danger uk-hidden"><span class="uk-icon uk-icon-image"><img src="../images/icons/ico-error.svg" alt=""/></span> Información requerida</div>
                            </div>
                            <div class="uk-margin">
                              <label class="uk-form-label">Unidad de la cantidad</label>
                              <div class="uk-form-controls">
                                <input class="uk-input uk-form-width-small" type="number" placeholder="ejm:unidades" required/>
										</div>
                                <div class="validation-error uk-text-danger uk-hidden"><span class="uk-icon uk-icon-image"><img src="../images/icons/ico-error.svg" alt=""/></span> Información requerida</div>
                                </div>
                                <div class="uk-margin">
                                  <label class="uk-form-label">Unidades en el inventario</label>
                                  <div class="uk-form-controls">
                                    <input class="uk-input uk-form-width-small" required type="number" placeholder="ejm:40" required/>
										</div>
                                    <div class="validation-error uk-text-danger uk-hidden"><span class="uk-icon uk-icon-image"><img src="../images/icons/ico-error.svg" alt=""/></span> Información requerida</div>
                                    </div>
                                    <div class="uk-margin">
                                      <label class="uk-form-label">Prioridad</label>
                                      <div class="uk-form-controls">
                                        <select class="uk-select uk-form-width-small" id="form-horizontal-select" required>
                                          <option>Escoger...</option>
                                          <option value="1">Option 01</option>
                                          <option value="2">Option 02</option>
                                        </select>
                                      </div>
                                      <div class="validation-error uk-text-danger uk-hidden"><span class="uk-icon uk-icon-image"><img src="../images/icons/ico-error.svg" alt=""/></span> Información requerida</div>
                                      </div>
                                    </div>
                                    <div class="uk-width-large">
                                      <legend class="uk-legend-small uk-text-gray uk-margin">Más detalles</legend>
                                      <div class="uk-margin uk-flex uk-flex-column">
                                        <label class="uk-form-label uk-width-large">Producto o recurso a solicitar</label>
                                        <div class="uk-form-controls uk-width-1-1 uk-margin-remove-left">
                                          <input class="uk-input" type="text" placeholder="ejm: guantes de cirugía" required/>
										</div>
                                          <div class="validation-error uk-text-danger uk-hidden"><span class="uk-icon uk-icon-image"><img src="../images/icons/ico-error.svg" alt=""/></span> Email es requerido</div>
                                          </div>
                                          <div class="uk-margin uk-flex uk-flex-column">
                                            <label class="uk-form-label uk-width-large">Comentarios adicionales</label>
                                            <div class="uk-form-controls uk-width-1-1 uk-margin-remove-left">
                                              <textarea class="uk-textarea" rows="5" placeholder="Detalles adicionales sobre la solicitud para que proveedor tenga más información..." required></textarea>
                                            </div>
                                            <div class="validation-error uk-text-danger uk-hidden"><span class="uk-icon uk-icon-image"><img src="../images/icons/ico-error.svg" alt=""/></span> Información requerida</div>
                                            </div>
                                          </div>
                                          <div class="uk-margin-medium uk-margin-small-bottom uk-text-center uk-width-1-1">
                                            <hr/>
                                              <button type="submit" class="uk-button uk-button-primary uk-button-large">Crear Solicitud</button>
								</div>

							</form>

                                        </div>

                                      </div>

                                    </div>

                                  </div>
    </>

  );
}

export default Create