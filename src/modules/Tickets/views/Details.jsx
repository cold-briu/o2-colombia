import React from 'react';
import icoCompletar from "../../../assets/icons/ico-completar-white.svg";
import icoTerminar from "../../../assets/icons/ico-terminar-white.svg";
import icoEliminar from "../../../assets/icons/ico-eliminar-white.svg";

const Details = () => {
  return (
    <>
      <div className="o2-main uk-section uk-section-small uk-section-default">

        <div className="uk-container uk-container-large">
          <ul className="uk-breadcrumb">
            <li><a href="#">Solicitudes</a></li>
            <li><span>Solicitud #65</span></li>
          </ul>
          <div className="o2-dashboard">
            <hr/>
              <div className="o2-header uk-flex uk-flex-between uk-flex-middle">
                <h1 className="uk-heading-small uk-text-aqua uk-margin-remove">
                  Solicitud #65
						</h1>
                <div className="uk-button-group uk-margin-remove uk-text-center" uk-margin=''>
                  <a className="uk-button uk-button-primary">
                    <img className="uk-icon" src={icoCompletar} alt=""/> Completar
							</a>
                    <a className="uk-button uk-button-info">
                      <img className="uk-icon" src={icoTerminar} alt=""/> Terminar
							</a>
                      <a className="uk-button uk-button-danger">
                        <img className="uk-icon" src={icoEliminar} alt=""/> Eliminar
							</a>
						</div>
					</div>
                    <hr/>



						<div className="o2-form-container uk-padding">

                        <form className="uk-form-stacked o2-form">
                          <div className="uk-width-expand">
                            <legend className="uk-legend-small uk-text-black uk-margin">Información básica</legend>
                            <div className="uk-margin uk-background-muted o2-details">
                              <div className="uk-flex uk-child-width-1-2@s uk-grid-collapse" uk-grid="">
                                <div className="o2-detail-row uk-grid-collapse" uk-grid="">
                                  <div className="uk-width-expand" uk-leader="">Estado</div>
                                  <div><span className="uk-label uk-label-info">Nuevo</span></div>
                                </div>
                                <div className="o2-detail-row uk-grid-collapse" uk-grid="">
                                  <div className="uk-width-expand" uk-leader="">Tipo de producto</div>
                                  <div className="uk-text-small">Protección Personal Asistencial</div>
                                </div>
                                <div className="o2-detail-row uk-grid-collapse" uk-grid="">
                                  <div className="uk-width-expand" uk-leader="">Solictud creada en</div>
                                  <div>03/20/2020</div>
                                </div>
                                <div className="o2-detail-row uk-grid-collapse" uk-grid="">
                                  <div className="uk-width-expand" uk-leader="">Última actualización</div>
                                  <div>Hace 4 días</div>
                                </div>
                                <div className="o2-detail-row uk-grid-collapse" uk-grid="">
                                  <div className="uk-width-expand" uk-leader="">Cantidad necesaria</div>
                                  <div>24 unidades</div>
                                </div>
                                <div className="o2-detail-row uk-grid-collapse" uk-grid="">
                                  <div className="uk-width-expand" uk-leader="">Unidades en el inventario</div>
                                  <div>10 unidades</div>
                                </div>
                                <div className="o2-detail-row uk-grid-collapse" uk-grid="">
                                  <div className="uk-width-expand" uk-leader="">Prioridad</div>
                                  <div><span className="uk-text-danger uk-text-uppercase uk-text-bold uk-text-small">Crítico</span></div>
                                </div>
                              </div>
                            </div>

                          </div>
                          <div className="uk-width-expand">
                </div>
                            <legend className="uk-legend uk-text-black uk-margin">Más detalles</legend>
                            <div className="uk-grid-collapse o2-details-comments" uk-grid="">
                              <div className="uk-width-1-3@s">
                                <div className="uk-card uk-card-small uk-card-default uk-card-body">
                                  <h3 className="uk-card-title uk-text-aquadark">Producto o recurso solicitado</h3>
                                  <p>Guantes de latex</p>
                                  <hr/>
                                    <h3 className="uk-card-title uk-text-aquadark uk-margin-remove-top"><span uk-icon="icon: commenting"></span> Comentarios</h3>
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt</p>
											</div>
                                </div>
                                <div className="uk-width-expand">
                                  <div className="uk-card uk-card-small uk-card-body">
                                    <ul className="uk-comment-list">
                                      <li>
                                        <article className="uk-comment uk-visible-toggle" tabIndex="-1">
                                          <header className="uk-comment-header uk-position-relative">
                                            <div className="uk-grid-medium uk-flex-middle" uk-grid="">
                                              <div className="uk-width-auto">
                                                <img className="uk-comment-avatar" src="images/avatar.jpg" width="80" height="80" alt=""/>
											                    </div>
                                                <div className="uk-width-expand">
                                                  <h4 className="uk-comment-title uk-margin-remove"><a className="uk-link-reset" href="#">Author</a></h4>
                                                  <p className="uk-comment-meta uk-margin-remove-top"><a className="uk-link-reset" href="#">12 days ago</a></p>
                                                </div>
                                              </div>
                                              <div className="uk-position-top-right uk-position-small uk-hidden-hover"><a className="uk-link-muted" href="#">Reply</a></div>
											            </header>
                                            <div className="uk-comment-body">
                                              <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                                            </div>
											        </article>
                                          <ul>
                                            <li>
                                              <article className="uk-comment uk-comment-primary uk-visible-toggle" tabIndex="-1">
                                                <header className="uk-comment-header uk-position-relative">
                                                  <div className="uk-grid-medium uk-flex-middle" uk-grid="">
                                                    <div className="uk-width-auto">
                                                      {/* TODO AVATAR */}
                                                      <img className="uk-comment-avatar" src="images/avatar.jpg" width="80" height="80" alt=""/>
											                            </div>
                                                      <div className="uk-width-expand">
                                                        <h4 className="uk-comment-title uk-margin-remove"><a className="uk-link-reset" href="#">Author</a></h4>
                                                        <p className="uk-comment-meta uk-margin-remove-top"><a className="uk-link-reset" href="#">12 days ago</a></p>
                                                      </div>
                                                    </div>
                                                    <div className="uk-position-top-right uk-position-small uk-hidden-hover"><a className="uk-link-muted" href="#">Reply</a></div>
											                    </header>
                                                  <div className="uk-comment-body">
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                                                  </div>
											                </article>
											            </li>
                                              <li>
                                                <article className="uk-comment uk-visible-toggle" tabIndex="-1">
                                                  <header className="uk-comment-header uk-position-relative">
                                                    <div className="uk-grid-medium uk-flex-middle" uk-grid="">
                                                      <div className="uk-width-auto">
                                                        <img className="uk-comment-avatar" src="images/avatar.jpg" width="80" height="80" alt=""/>
											                            </div>
                                                        <div className="uk-width-expand">
                                                          <h4 className="uk-comment-title uk-margin-remove"><a className="uk-link-reset" href="#">Author</a></h4>
                                                          <p className="uk-comment-meta uk-margin-remove-top"><a className="uk-link-reset" href="#">12 days ago</a></p>
                                                        </div>
                                                      </div>
                                                      <div className="uk-position-top-right uk-position-small uk-hidden-hover"><a className="uk-link-muted" href="#">Reply</a></div>
											                    </header>
                                                    <div className="uk-comment-body">
                                                      <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                                                    </div>
											                </article>
											            </li>
											        </ul>
                                            </li>
                                          </ul>
											</div>
									</div>
								</div>


							</form>

                                </div>

                              </div>

                            </div>

                          </div>
    </>
  );
}

export default Details