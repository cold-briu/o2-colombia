import Dashboard from "./Dashboard";
import Create from "./Create";
import Details from "./Details";

export { Dashboard, Create, Details }
