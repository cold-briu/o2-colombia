import * as views from './views'
import * as services from './services'

export default { views, services }