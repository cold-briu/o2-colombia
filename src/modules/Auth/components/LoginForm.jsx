import React from 'react'
import { constants } from '../../../router'
const { routes } = constants
const LoginForm = ()=>{
  return (
    <>
      <div className="o2-login-institutional uk-padding">
				<form className="uk-form-stacked o2-form login-box">
          <legend className="uk-legend uk-text-center uk-text-primary uk-heading-divider uk-margin">Bienvenido</legend>
          <div className="uk-margin">

            <label className="uk-form-label">Email</label>

            <div className="uk-form-controls uk-inline uk-width-1-1">
              <span className="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: mail; ratio: 1.25"></span>
              <input id="email" className="uk-input uk-form-large form-control"
                     type="email" name="email" value=""
                     autocomplete="email" placeholder=""
                     autofocus />
            </div>

            <div className="validation-error uk-text-danger uk-hidden">
              <span className="uk-icon uk-icon-image">
                <img src="../images/icons/ico-error.svg" alt=""/>
              </span>
            </div>

					</div>


          <div className="uk-margin">

            <label className="uk-form-label">Contraseña</label>

            <div className="uk-form-controls uk-inline uk-width-1-1">
              <span className="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: lock; ratio: 1.25"></span>
              <input id="password" className=" uk-input uk-form-large form-control"
                    type="password" name="password" value=""
                    autocomplete="" placeholder=""  />
            </div>
						<div className="validation-error uk-text-danger uk-hidden">
              <span className="uk-icon uk-icon-image">
                <img src="../images/icons/ico-error.svg" alt=""/>
              </span>
            </div>

          </div>


					<div className="uk-margin">
            <label className="form-check-label" for="remember">
              <input className="uk-checkbox form-check-input" type="checkbox" name="remember" id="remember"/>Recordarme</label>
          </div>


          <div className="uk-margin-medium uk-margin-small-bottom uk-text-center">
            {/* <button type="submit" className="uk-button uk-button-primary uk-button-large">Ingresar</button> */}
            <a href={routes.dashboard} className="uk-button uk-button-primary uk-button-large">Ingresar</a>
          </div>

          <div className="uk-margin uk-text-center">
            <a className="uk-button uk-button-link" href="#">
              Recordar mi contraseña <span uk-icon="icon: arrow-right"></span>
            </a>
          </div>

			  </form>
		  </div>
    </>
  )

}
export default LoginForm