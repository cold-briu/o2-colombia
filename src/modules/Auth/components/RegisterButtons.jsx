import React from 'react'
import icoInstitute from '../../../assets/icons/ico-institucion.svg'
import icoProvider from '../../../assets/icons/ico-proveedor.svg'
import config from '../../../config'
const { registroProveedores } = config.appLinks.kompras
const RegisterButtons = ()=>{
  const handleClick = () => {
    alert('Serás re-dirigido a Kompras BPO, nuestro partner OFICIAL de proveedores.');
    window.location.href = registroProveedores;
  }

  return (
    <>

    <div className="o2-register-intro uk-width-1-1@m">
    <div className="uk-card uk-card-small uk-flex-center uk-grid-collapse" uk-grid>

      <div className="uk-width-auto">
        <div className="uk-card-body uk-text-center">
          <h4 className="uk-margin-small uk-text-graydarkx uk-text-body uk-text-bold uk-text-uppercase ">
            ¿Aún no tienes cuenta en O2Colombia? <br/>
            <label className="uk-text-small uk-text-uppercase uk-text-graydarkx ">
              Escoge tu perfil para participar:
            </label>
					</h4>
				</div>
      </div>

      <div className="o2-register-buttons uk-width-1-1@m">
        <div className="uk-card uk-card-body uk-text-center">
          <div className="o2-buttongroup uk-button-group uk-margin-small">
            <div>
              <a href="/register"  className="uk-button uk-button-small">
                <img src={icoInstitute} alt=""/><br/>
                Institución
							</a>
              <button onclick={handleClick} className="uk-button uk-button-small">
                <img src={icoProvider} alt=""/><br/>
                Proveedor
							</button>
						</div>
          </div>
				</div>
			</div>

      <div className="uk-width-1-1 uk-background-default">
        <div className="uk-card uk-card-body uk-text-center">
          <p className="uk-margin uk-text-graydarkx uk-text-body uk-text-bold uk-text-uppercase">
            ¿Necesitas ayuda?
            <a target="_blank" href="mailto:info@o2colombia.com?subject=Soporte" className="uk-button uk-button-default uk-button-small">
              Escríbenos
            </a>
          </p>
        </div>
      </div>
    </div>
    </div>
    </>
  )

}
export default RegisterButtons