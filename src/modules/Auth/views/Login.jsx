import React from 'react';
import * as services from '../services'

import { LoginForm, RegisterButtons} from '../components'

import shared from "../../Shared";

const { Footer, Header } = shared.components;


const Login = () => {


  const auth = async ()=>{
    const res = await  services.user.setIsAuth()
    if (res && res.success) {
      document.querySelector('#redirect').click()
    }
  }
  return (
    <>
      <a href="/dashboard" id="redirect" hidden></a>



        <div style={{"height":"100%"}} className="o2-main uk-section uk-section-small uk-section-default uk-padding-remove">
          <div className="uk-container uk-container-expand uk-padding-remove">
            <div className="o2-form-container uk-child-width-1-1@m uk-flex-center uk-grid-collapse uk-grid-match " uk-grid>
              <LoginForm/>
            </div>
            <div className="o2-register-box uk-background-muted uk-flex-middle uk-grid-collapse uk-grid-match" uk-grid>

                  <RegisterButtons/>

            </div>



          </div>
        </div>




    </>

  );
}

export default Login