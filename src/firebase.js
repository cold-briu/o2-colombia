import * as firebase from 'firebase';
// import * as admin from 'firebase-admin'

// import firebase from 'firebase/app';
// import firestore from 'firebase/firestore'



import config from './config'

firebase.initializeApp(config.firebase);

firebase.firestore().settings({});

export default firebase;