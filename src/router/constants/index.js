export const routes = {
  home: '/home',
  login: '/login',
  dashboard: '/tickets',
  create: '/tickets/create',
  details: '/tickets/details',
}