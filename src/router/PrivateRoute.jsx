import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Shared from "../modules/Shared";

// import Navbar from '../components/Navbar.component'
// import Tabsbar from '../components/Tabsbar.component'

// import userAuth from '../userAuth';

const { UserLayout } = Shared.layouts

const PrivateRoute = ({ component: Component, ...rest }) => {

  //    let isAuth = localStorage.getItem('userAuth');
  // let isAuth = userAuth.getEmail()

  return (
    <Route {...rest} render={matchProps => (
      <>
        {/* <Navbar /> */}
        {
          true ?
            <UserLayout>
              <Component {...matchProps} />
            </UserLayout>
            :
            <Redirect to="/login" />
        }
        {/* <Tabsbar /> */}
      </>
    )} />
  )
}



export default PrivateRoute