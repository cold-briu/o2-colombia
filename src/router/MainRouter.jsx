import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import PrivateRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'

import LandingModule from '../modules/Landing'
import TicketsModule from '../modules/Tickets'
import AuthModule from '../modules/Auth'

import { routes } from "./constants";

const { Landing } = LandingModule.views
const { Dashboard, Create, Details } = TicketsModule.views
const { Login } = AuthModule.views

const { user: userService } = AuthModule.services


export default class MainRouter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSignedIn: false,

    }
  }
  render() {
    const isSignedIn = userService.getIsAuth()
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path={routes.login}>
            {
              (false ||isSignedIn) ?
              <Redirect to={routes.dashboard} />
                :
                <PublicRoute exact path={routes.login} component={Login} />
            }
          </Route>
          <Route exact path={routes.home} component={Landing} />
          <PrivateRoute exact path={routes.dashboard} component={Dashboard} />
          <PrivateRoute exact path={routes.create} component={Create} />
          <PrivateRoute exact path={routes.details} component={Details} />
          <Route >
            <Redirect to={routes.home} />
          </Route>

        </Switch>
      </BrowserRouter>
    )
  }
}
