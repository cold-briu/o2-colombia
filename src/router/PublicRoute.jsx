import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Shared from "../modules/Shared";



const { PublicLayout } = Shared.layouts

const PublicRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={matchProps => (
      <>
        <PublicLayout>
          <Component {...matchProps} />
        </PublicLayout>
      </>
    )} />
  )
}



export default PublicRoute