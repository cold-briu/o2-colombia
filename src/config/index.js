export default {
  api: {
    host: process.env.REACT_APP_BASE_URL
  },
  firebase: {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: "o2-colombia.firebaseapp.com",
    databaseURL: "https://o2-colombia.firebaseio.com",
    projectId: "o2-colombia",
    storageBucket: "o2-colombia.appspot.com",
    messagingSenderId: process.env.REACT_APP_messagingSender,
    appId: process.env.REACT_APP_APP_ID,
    measurementId: process.env.REACT_APP_MEASUREMENT_ID,
  },
  appLinks:{
    kompras:{
      registroProveedores: 'https://portal.komprasbpo.com/c19/Admin/Company/Register'
    }
  }
}
